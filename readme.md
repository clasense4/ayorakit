#Installation
1. `cd /var/www && git clone `
2. `cd /var/www/ayorakit/ && composer install`
3. `cd /var/www/ayorakit/web/ && bower install`
4. Create Database
5. Change file `/var/www/app/build/build.properties runtime-conf.xml`
6. `cd /var/www/ayorakit/app/build && propel-gen` to build the model
7. `cd /var/www/ayorakit/app/build/propel-gen insert-sql` to insert data to database
8. To insert sample data, there is `/var/www/ayorakit/app/build/data.xml` use `propel-gen datasql` to convert it into sql file

