<?php

namespace scrapy_rakitan;
use Symfony\Component\HttpFoundation\Response;
use Propel;
use Redis;
use PDO;
// use Domino\CacheStore\Factory;

/**
* Class Ayo Rakit
*/
class AyoRakit
{
	public function test()
	{
		return "test";
	}

	/**
	 * Get data from redis,
	 * if not cache, query then cache
	 * SELECT FROM RAKITAN ONLY !!!
	 * @param  [array] $array [array of string]
	 * @return [array]        [array of result]
	 */
	public static function getData($array)
	{
	    // set redis Cache
	    $r = new RedisSilex();
	    $r->prepare('127.0.0.1', 6354);
	    $key = implode(',',$array);
	    $key = "cache:".$key;

	    // Cek Query Exists
		// echo self::printr($r);
		// echo self::printr($key);
		if($r->redis->exists($key)){
			// self::printr($r);
			// echo "Get from Redis";
			$un_ser_datas = $r->redis->get($key);
			$datas = unserialize($un_ser_datas);
			return $datas;
		}
		else {
			// echo "write to Redis";
			// Query to DB
		    $components = Model\RakitanQuery::create()
		        ->filterByItemCategoryHash($array)
		        ->groupByItemNameHash()
		        ->orderByItemName()
		        ->find();

		    // set var
		    $datas = [];

		    //set default var
		    $datas['default'] = 'Please Choose';
		    foreach ($components->toArray() as $value) {
		        $datas[$value['ItemNameHash']] = $value['ItemName'];
		    }
			
			// serialize data
			$ser_data = serialize($datas);
			// echo $ser_data;
			// Save Array to Redis
			if ($r->redis->set($key, $ser_data)){
				// echo "Cached ";
				$r->redis->setTimeout($key,600);
			}
			return $datas;

		}

		// Factory::setOption(
		//     array(
		//         'storage'     => 'memcached',
		//         'prefix'      => 'domino_test',
		//         'default_ttl' => 360,
		//         'servers'     => array(
		//             array('127.0.0.1', 11211, 20)
		//         )
		//     )
		// );
		
		// return("success");
	    // select item_name from rakitan
	    // where item_category_hash in ('4758924ade','365e4ec0cc')
	    // group by item_name_hash
	    // Create Query Builder
	    // echo $key;
	    // die;
	}

	public static function API_getComponent($categoryHash)
	{
	    $components = Model\RakitanQuery::create()
	        ->filterByItemCategoryHash($categoryHash)
	        ->select('ItemName')
	        ->groupByItemNameHash()
	        ->orderByItemName()
	        ->find();
	    return new Response(json_encode($components->toArray()), 200, array (
	        'Content-Type' => 'application/json'
	    ));
	    // return json_encode($components->toArray());
	}

	/**
	 * Simple print_r with <pre>
	 * @param  $array [any array]
	 * @return String [Formatted]
	 */
	public static function printr($array)
	{
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}


	/**
	 * Get data By Custom SQL Query
	 * @param  [String]  $sql         [the sql Query]
	 * @param  boolean $remove_keys [description]
	 * @param  [String]  $dbName      [Database name, default to DBNAME Constant, see bootstap.php]
	 * @return [array] $res              [array of result from the database]
	 */
	public static function getDataBySql($sql, $remove_keys=FALSE, $dbName=DBNAME) 
	{	
		$con = Propel::getConnection($dbName);
		$stmt = $con->prepare($sql); 
		$stmt->execute(); 
		if ($remove_keys) {
			$result = $stmt->setFetchMode(PDO::FETCH_NUM);		
			$res = array();
			while ($row = $stmt->fetch()) {
				$res[] = $row;
			}		
		} else {		
			$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);		
			$res = $stmt->fetchAll();		
		}
		
		return $res;
	}


	/**
	 * Number Format
	 * < 50000
	 * > 50,000
	 * @param  [Integer] $number [the Number that want to format]
	 * @return [String]         [Formatted Number]
	 */
	public static function nf($number) {
		return number_format($number, 0, '', '.');
	}


	/**
	 * Cut Words for long words make it simpler
	 * @param  String  $string [The Long Words]
	 * @param  integer $char  [char count]
	 * @param  integer $word  [word count]
	 * @return String          [short string]
	 */
    public static function cutWords($string, $char = 10, $word = 5){
        $string = wordwrap($string, $char, ";;", true);
        $res = explode(";;", $string);
        $ret = "";
        for ($i = 0; $i < $word; $i++) {
            isset($res[$i]) ? $ret .= " ".$res[$i] : "";
        }
        return $ret;
    }	
}