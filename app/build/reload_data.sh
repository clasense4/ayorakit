# 1. Create the backup first
propel-gen datadump
# 2. Generate the new model
propel-gen
# 3. Convert data dump into sql
propel-gen datasql
# 4. Insert the sql into database
propel-gen insert-sql
