<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;
use scrapy_rakitan\Model as S;
// use redis_connection as R;
// use ayorakits;
// use ayorakit;
// use ayorakits as A;

$app = require_once __DIR__ . '/bootstrap.php';

Debug::enable();

/**
 * Enable Silex Error
 */
// $app->error(function (\Exception $e, $code) {
//     switch ($code) {
//         case 404:
//             $message = 'The requested page could not be found.';
//             break;
//         default:
//             $message = 'We are sorry, but something went terribly wrong.';
//     }

//     return new Response($message);
// });

// $app->get('/hello/{name}', function ($name) use ($app) {

//     return 'Hello '.$app->escape($name);

// });


/**
 * Get Some Devs ID
 */
$app->get('/User/{id}', function ($id) use ($app) {
    // $r = S\RedisSilex::getCache("TESTKEY");
    $r = new S\RedisSilex();
    // $redis->prepare('localhost', 6354);
    // $r->prepare('127.0.0.1', 6354);
    $r->prepare('127.0.0.1', 6354);
    // echo "<hr>";
    $json = $r->getCache('cache:a950d7d8135b48965d0c324b4e358e71');
    // print_r($redis);
    $con = Propel::getConnection(S\UserPeer::DATABASE_NAME);
    $con->useDebug(true);

    // $d = S\UserPeer::retrieveByPK($id);
    // $key = "cache:". md5($con->getLastExecutedQuery());
    // get data from redis
    // $datas = $app['redis']->get($key);
    // print_r($datas);
    // $app['redis']->set($key,$d->toJSON());
    // return $d->toJSON();
    return new Response($json, 200, array (
        'Content-Type' => 'application/json'
    ));
});


/**
 * Form definition
 */
$app->match('/form', function (Request $request) use ($app) {
    // kick if not authenticated
    // if (null === $user = $app['session']->get('user')) {
    //     return $app->redirect('login');
    // }
    // some default data for when the form is displayed the first time

    // $item_categories = S\ItemCategoryPeer::retrieveByPK(4);
    // $item = $item_categories->toArray()['category_name'];
    // echo $item;
    // print_r($app);
    // $test = new S\RedisSilex();

    // set var
    $array = [];
    $data = [];

    // PROCESSOR
    $array['processor'] = array('4758924ade','365e4ec0cc');
    $data['processor'] = scrapy_rakitan\AyoRakit::getData($array['processor']);
    // print_r($data);

    // MOTHERBOARD
    $array['motherboard'] = array('2b80524390','fe54e40b7c');
    $data['motherboard'] = scrapy_rakitan\AyoRakit::getData($array['motherboard']);
    // print_r($data['motherboard']);

    // VGA
    $array['vga'] = array('0c350446c0','e52f0db2c3');
    $data['vga'] = scrapy_rakitan\AyoRakit::getData($array['vga']);

    // MEMORY DDR3
    $array['ddr3'] = array('f584aa2bf7');
    $data['ddr3'] = scrapy_rakitan\AyoRakit::getData($array['ddr3']);

    // PSU > 1000 , < 900, < 600, < 400
    $array['psu'] = array('3463be53b6', 'fdc7c84d0e', '9071f6b693', 'f6cf08a99b');
    $data['psu'] = scrapy_rakitan\AyoRakit::getData($array['psu']);

    // Casing Full tower , middle Tower, mini ATX, < 400
    $array['casing'] = array('b2b5a68140', '469c6b964f', 'fad69959db');
    $data['casing'] = scrapy_rakitan\AyoRakit::getData($array['casing']);

    // Hard disk 3.5'
    $array['hdd'] = array('08f54bde03');
    $data['hdd'] = scrapy_rakitan\AyoRakit::getData($array['hdd']);

    // Optical Disk Drive
    $array['odd'] = array('4095eb019b');
    $data['odd'] = scrapy_rakitan\AyoRakit::getData($array['odd']);

    // Monitor
    $array['monitor'] = array('f9134e57cd','bbfb72b151');
    $data['monitor'] = scrapy_rakitan\AyoRakit::getData($array['monitor']);

    // Cooler
    $array['cooler'] = array('906ead17b1');
    $data['cooler'] = scrapy_rakitan\AyoRakit::getData($array['cooler']);

    // UPS
    $array['ups'] = array('79f607d587');
    $data['ups'] = scrapy_rakitan\AyoRakit::getData($array['ups']);

    // Keyboard & mouse, Keyboard, Mouse
    $array['km'] = array('4702bf4f3b');
    $data['km'] = scrapy_rakitan\AyoRakit::getData($array['km']);

    // FORM PROJECT
    $formProject = $app['form.factory']->createBuilder('form')
        ->add('project_name', 'text', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Project Name', 'value' => '')
        ))
        ->getForm();

    $form = $app['form.factory']->createBuilder('form')
        ->add('processor', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Processor', 'value' => ''),
            'choices' => $data['processor'],
            'preferred_choices' => array('default')
        ))
        ->add('motherboard', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Motherboard', 'value' => ''),
            'choices' => $data['motherboard'],
            'preferred_choices' => array('default')
        ))
        ->add('vga', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'VGA', 'value' => ''),
            'choices' => $data['vga'],
            'preferred_choices' => array('default')
        ))
        ->add('Memory_ddr_3', 'choice', array(
            'label' => 'Memory DDR3',
            'attr' => array('class' => 'form-control', 'placeholder' => 'Memory DDR3', 'value' => ''),
            'choices' => $data['ddr3'],
            'preferred_choices' => array('default')
        ))
        ->add('psu', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'PSU', 'value' => ''),
            'choices' => $data['psu'],
            'preferred_choices' => array('default')
        ))
        ->add('casing', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Casing', 'value' => ''),
            'choices' => $data['casing'],
            'preferred_choices' => array('default')
        ))
        ->add('hdd', 'choice', array(
            'label' => 'Hard Disk 3.5"',
            'attr' => array('class' => 'form-control', 'placeholder' => 'HDD', 'value' => ''),
            'choices' => $data['hdd'],
            'preferred_choices' => array('default')
        ))
        ->add('odd', 'choice', array(
            'label' => 'Optical Disk Drive',
            'attr' => array('class' => 'form-control', 'placeholder' => 'ODD', 'value' => ''),
            'choices' => $data['odd'],
            'preferred_choices' => array('default')
        ))
        ->add('monitor', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Monitor', 'value' => ''),
            'choices' => $data['monitor'],
            'preferred_choices' => array('default')
        ))
        ->add('cooler', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Cooler', 'value' => ''),
            'choices' => $data['cooler'],
            'preferred_choices' => array('default')
        ))
        ->add('ups', 'choice', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'UPS', 'value' => ''),
            'choices' => $data['ups'],
            'preferred_choices' => array('default')
        ))
        ->add('km', 'choice', array(
            'label' => 'Keyboard & Mouse',
            'attr' => array('class' => 'form-control', 'placeholder' => 'KM', 'value' => ''),
            'choices' => $data['km'],
            'preferred_choices' => array('default')
        ))
        ->add('project_id', 'hidden', array(
            // 'label' => 'Keyboard & Mouse',
            'attr' => array('class' => 'form-control', 'placeholder' => 'Project Id', 'value' => ""),
            // 'choices' => $data['km'],
            // 'preferred_choices' => array('default')
        ))
        ->getForm();

        unset($array);
        unset($data);

        // POST for Create Project
        if ('POST' == $request->getMethod()) {
            $formProject->bind($request);
            // echo "POST";
            if ($formProject->isValid()) {
                $datas = $formProject->getData();

                // do something with the data
                // echo scrapy_rakitan\AyoRakit::printr($datas);
                // redirect somewhere
                // return $app->redirect('...');

                // Insert into database
                $projects = new S\Project();
                $projects->setName($datas['project_name']);
                $projects->setUserId($user['user_id']);
                $projects->save();

                // set the var
                $project_id = $projects->getProjectId();

                // add a form project id in our form before
                $form->add('project_id', 'text', array(
                        'attr' => array('class' => 'form-control', 'placeholder' => 'Project ID', 'value' => $project_id)
                )) ;
                // $formProjectId = $app['form.factory']->createBuilder('form')
                //     ->add('project_id', 'text', array(
                //         'attr' => array('class' => 'form-control', 'placeholder' => 'Project ID', 'value' => $project_id)
                //     ))
                //     ->getForm();
                // echo scrapy_rakitan\AyoRakit::printr($project_id);

                // If project  inserted, show list form
                return $app['twig']->render('form.twig',
                    array (
                        'form' => $form->createView(),
                        // 'form_project_id' => $formProjectId->createView(),
                        // 'form_project' => $formProject->createView()
                    )
                );
            }
        }

    // display the form (if not submitted the project)
    return $app['twig']->render('form.twig',
        array (
            // 'form' => $form->createView(),
            'form_project' => $formProject->createView()
        )
    );
});

/**
 * Form Result
 */
$app->post('/form/result', function(Request $request) use ($app) {
    // get all data from post request
    $res = $request->request->all();
    echo scrapy_rakitan\AyoRakit::printr($res);

    $project_id = $res['form']['project_id'];
    // echo "project_id = ".$project_id."<br>";
    foreach ($res['form'] as $key => $value) {
        // echo scrapy_rakitan\AyoRakit::printr($value);

        if (($value != 'default') && ($key <> '_token') && ($key <> 'project_id') ){
            // echo $key . "=>" . $value."<br>";
            $listItem = new S\ListItem();
            $listItem->setProjectId($project_id);
            $listItem->setItemNameHash($value);
            if($listItem->save()){
                echo "sukses<br>";
            }
        }
    }
    // $list = new S\List();
    // $list->set
    // echo scrapy_rakitan\AyoRakit::printr($list);
    // if (!$event = $request->get('email')){
    //     $app->abort(400, 'missing parameters');
    // }
    return "";
});

/**
 * Hello with Twig
 */
$app->get('/hello/{name}', function ($name) use ($app) {
    return $app['twig']->render('hello.twig', array(
        'name' => $name,
    ));
});

/**
 * LOGIN
 */
$app->get('/login', function () use ($app) {
    // $username = $app['request']->server->get('PHP_AUTH_USER', false);
    // $password = $app['request']->server->get('PHP_AUTH_PW');

    // Cek session, If exists then redirect
    if (! (null === $user = $app['session']->get('user'))) {
        return $app->redirect('account');
    }
    // echo "login";
    // return "";
    $form = $app['form.factory']->createBuilder('form')
        ->add('Email', 'email', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Email', 'value' => ''),
        ))
        ->add('Password', 'password', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Password', 'value' => ''),
        ))
    ->getForm();

    $response = new Response ($app['twig']->render('login.twig', array('form' => $form->createView() ) ) );
    // $response->setTtl(100);
    return $response;
    // return $app['twig']->render('login.twig');
    //
    // $app->get('/page-with-cache', function() use ($app) {
        // $response = new Response($app['twig']->render('page-with-cache.html.twig', array('date' => date('Y-M-d h:i:s'))));
        // $response->setTtl(10);

        // return $response;
    // })->bind('page_with_cache');
})->bind('login');

/**
 * LOGIN RESULT
 */
$app->post('/login/result', function (Request $request) use ($app) {
    $res = $request->request->all();
    // echo scrapy_rakitan\AyoRakit::printr($res);

    $users = S\UserQuery::create()
        ->filterByEmail($res['form']['Email'])
        ->filterByPassword($res['form']['Password'])
        ->find();

    // echo scrapy_rakitan\AyoRakit::printr($users->toArray());
    // die;
    $username = $users->toArray()[0]['Username'];
    $user_id = $users->toArray()[0]['UserId'];
    // $username = $username['Username'];
    if (1 === count($users->toArray())) {
        $app['session']->set('user',
            array (
                'username' => $username,
                'user_id' => $user_id
            )
        );
        return $app->redirect('../account');
    }

    $response = new Response();
    $response->headers->set('WWW-Authenticate', sprintf('Basic realm="%s"', 'site_login'));
    $response->setStatusCode(401, 'Please sign in.');
    return $response;
});

/**
 * LOGOUT
 */
$app->get('/logout', function (Request $request) use ($app) {
    $app['session']->clear();
    // $app['session']->setFlash('msg', 'logged out.');
    return $app->redirect('login');
});

/**
 * ACCOUNT
 */
$app->get('/account', function () use ($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $data = array(
        "username" => $user['username'],
        "user_id" => $user['user_id']
    );

    // Get user project
    $project = S\ProjectQuery::Create()
        ->filterByUserId($data['user_id'])
        ->find();

    // echo scrapy_rakitan\AyoRakit::printr($project);
    $data['projects'] = $project->toArray();
    $project_id = $data['projects'][0]['ProjectId'];
    // get list project
    $listItem = S\ListItemQuery::Create()
        ->filterByProjectId($project_id)
        ->find();

    // echo scrapy_rakitan\AyoRakit::printr($listItem->toArray());
    $sql = "select r.item_category as 'Category', r.item_name as 'Name',DATE(r.timestamp) as 'Date', r.item_price as 'Price'
            from list_item l
            left join rakitan r on l.item_name_hash = r.item_name_hash
            where DATE(r.timestamp) = subdate(current_date,0)
            and project_id=".$project_id."
            UNION
            select 'Total','','', SUM(r.item_price) as 'TOTAL' from list_item l
            left join rakitan r on l.item_name_hash = r.item_name_hash
            where DATE(r.timestamp) = subdate(current_date,0)
            and project_id=".$project_id."";

    $list = scrapy_rakitan\AyoRakit::getDataBySQL($sql);
    // echo scrapy_rakitan\AyoRakit::printr($list);

    $data['items'] = $list;
    return $app['twig']->render('account.twig', $data);
    // return "";
});


/**
 * PROJECT
 * /project/project_id_hash
 */
$app->get('/project/{id}', function ($id) use ($app) {
    // if (null === $user = $app['session']->get('user')) {
    //     return $app->redirect('login');
    // }

    // $data = array(
    //     "username" => $user['username'],
    //     "user_id" => $user['user_id']
    // );

    // Get user project
    // $project = S\ProjectQuery::Create()
    //     ->filterByUserId($id)
    //     ->findOne();

    // echo scrapy_rakitan\AyoRakit::printr($project);
    // $data['projects'] = $project->toArray();

    // set Var
    $project_id = $id;

    // Get project Info
    $project = S\ProjectQuery::Create()
        ->findPK($id);

    // check count result
    if (count($project) == 0 ){
        $data['project_name'] = "";
        return $app['twig']->render('project.twig', $data);
    }

    $projects = $project->toArray();
    // echo scrapy_rakitan\AyoRakit::printr($projects);

    // prepare Query
    $sql = "select r.item_category as 'Category', r.item_name as 'Name',DATE(r.timestamp) as 'Date', r.item_price as 'Price'
            from list_item l
            left join rakitan r on l.item_name_hash = r.item_name_hash
            where DATE(r.timestamp) = subdate(current_date,0)
            and project_id=".$project_id."
            UNION
            select 'Total','','', SUM(r.item_price) as 'TOTAL' from list_item l
            left join rakitan r on l.item_name_hash = r.item_name_hash
            where DATE(r.timestamp) = subdate(current_date,0)
            and project_id=".$project_id."";
    $list = scrapy_rakitan\AyoRakit::getDataBySQL($sql);
    // echo scrapy_rakitan\AyoRakit::printr($list);

    // format Name to make it simpler
    // format Price to make it have comma separated
    for ($i = 0; $i < count($list); $i++){
        // echo scrapy_rakitan\AyoRakit::printr($value);
        $list[$i]['Name'] = scrapy_rakitan\AyoRakit::cutWords($list[$i]['Name']);
        $list[$i]['Price'] = scrapy_rakitan\AyoRakit::nf((int) $list[$i]['Price']);
    }
    // echo scrapy_rakitan\AyoRakit::printr($list);

    $data['items'] = $list;
    $data['project_name'] = $projects['name'];
    return $app['twig']->render('project.twig', $data);
    // return "";
});

$app->get('/registration', function() use ($app) {
    // FORM REGISTRATION
    $formRegistration = $app['form.factory']->createBuilder('form')
        ->add('user_name', 'text', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'ex: clasense4', 'value' => '')
        ))
        ->add('email', 'email', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'ex: clasense4@gmail.com', 'value' => '')
        ))
        ->add('password', 'password', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'Y0uR_p4ssWD', 'value' => '')
        ))
        ->getForm();

    return $app['twig']->render('registration.twig',
        array(
            // 'echo' => 'registration',
            'form_registration' => $formRegistration->createView(),
        )
    );
});

/**
 * Sample
 */
$app->get('/', function() use ($app) {
    return $app['twig']->render('index.twig');
});

/**
 * Middleware for API
 */
$before = function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
    else {
        die("whoops");
    }
};


/**
 * API
 * Get a list of item, based on category
 * {"cat":"MB Intel 1155", "items":["Asus 970fx", "MSI 990FX"]}
 */
$app->get('/API/cat/{model}', function (Request $request, $model) use ($app) {
    // echo $model;
    // check switch case
    switch ($model) {
        case 'amd_am3':
            $component = scrapy_rakitan\AyoRakit::API_getComponent('4758924ade');
            break;
        case 'amd_fm2':
            $component = scrapy_rakitan\AyoRakit::API_getComponent('365e4ec0cc');
            break;
        default:
            # code...
            break;
    }
    return $component;
});


/**
 * ABOUT PAGE
 */
$app->get('/about', function() use ($app){
    // return "this is about";
    return $app['twig']->render('about.twig');
});


/**
 * CONTACT PAGE
 */
$app->get('/contact', function() use ($app){
    $formContact = $app['form.factory']->createBuilder('form')
        ->add('name', 'text', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'ex: Fajri Abdillah', 'value' => '')
        ))
        ->add('email', 'email', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'ex: clasense4@gmail.com', 'value' => '')
        ))
        ->add('message', 'textarea', array(
            'attr' => array('class' => 'form-control', 'placeholder' => 'ex: Your messages', 'value' => '')
        ))
        ->getForm();
    return $app['twig']->render('contact.twig', array('form' => $formContact->createView() ));
});



/**
 * Sample Post Routing
 * {"user":"fajri", "password":"54321"}
 */
// $app->post('/json/login', function (Request $request) use ($app) {
//     // check if all parameters is included
//     if ( 2 == count($request->request->all()) ){
//         $post = array(
//             'user' => $request->request->get('user'),
//             'password' => $request->request->get('password')
//         );

//         $user = A\UserDeveloperQuery::create()
//           ->filterByUsername($post['user'])
//           ->filterByPassword($post['password'])
//           ->findOne();
//         // print_r($user);
//         $text = "Welcome " . $user->getName();
//         return $app->json($text, 201);
//     }
//     else {
//         return "Whoops, need some params :)";
//     }

// })->before($before);


/**
 * Get Some Devs ID
 */
// $app->get('/Devs/{id}', function ($id) use ($app) {
//     $d = A\UserDeveloperPeer::retrieveByPK($id);
//     // return $d->toJSON();
//     return new Response($d->toJSON(), 200, array (
//         'Content-Type' => 'application/json'
//     ));
// });

// $app['http_cache']->run();
return $app;


?>