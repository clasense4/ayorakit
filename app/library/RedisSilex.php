<?php
namespace scrapy_rakitan\Model;

use Redis;

class RedisSilex
{
	/**
	 * redis key
	 * @var string
	 */
	protected $key;

	/**
	 * redis port
	 * @var string
	 */
	protected $port;

	/**
	 * redis host
	 * @var string
	 */
	protected $host;

    /**
     * redis Object
     * @var object
     */
    public $redis;

    /**
     * Gets the redis key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Sets the redis key.
     *
     * @param string $key the key
     *
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Gets the redis port.
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Sets the redis port.
     *
     * @param string $port the port
     *
     * @return self
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    public function __construct()
    {
        // $this->redis = new stdClass;
    }
    /**
     * Gets the redis host.
     *
     * @return strring
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets the redis host.
     *
     * @param strring $host the host
     *
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }


    public function prepare($host, $port)
    {
        $this->setHost($host);
        $this->setPort($port);
        $redis = new Redis() or die("Gagal memuat modul redis.");
        $redis->connect($this->host, $this->port);
        $this->redis = $redis;
        // $this->setHost($host);
        // $this->setPort($port);

        return $this;
    }


    public function getCache($key)
    {
        // try {
        //     print_r($this->redis);
        // }
        // catch (\Exception $e){
        //     die("whoops");
        // }
        // echo $key;
        return $this->redis->get($key);
    }
}