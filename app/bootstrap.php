<?php

// APP Name (move this later)
define ('APPNAME', 'scrapy_rakitan');
define ('DBNAME', 'scrapy_rakitan');

// Prepare Class Environment 
$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('scrapy_rakitan', __DIR__.'/../src/');
$loader->add('ayorakits', __DIR__.'/library/');
// print_r($loader);
// $loader->add('redis_connection', __DIR__.'/redis_connection.php');

// Initialize the App
$app = new Silex\Application();
$app['debug'] = true;

// Cache
$app['cache.path'] = __DIR__ . '/cache';

// Http cache
$app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

// Twig cache
$app['twig.options.cache'] = $app['cache.path'] . '/twig';

// Load Redis setting
$app['redis'] = new Redis() or die("Gagal memuat modul redis.");
$app['redis']->connect('127.0.0.1', 6354);

//  Propel 
$app['propel.config_file'] = __DIR__.'/build/conf/scrapy_rakitan-conf.php';
$app['propel.model_path'] = __DIR__.'/../src/';
$app->register(new Propel\Silex\PropelServiceProvider());

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../web',
    // 'twig.cache' => $app['twig.options.cache'],
    'twig.options' => array('cache' => $app['twig.options.cache']),
));

// Register Session
$app->register(new Silex\Provider\SessionServiceProvider());

// Register for auth too
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// use FormServiceProvider;
$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());

// $app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
//     'http_cache.cache_dir' => $app['http_cache.cache_dir'],
//     'http_cache.esi'       => null,
// ));

// Set TIMEZONE
date_default_timezone_set('Asia/Jakarta');

return $app;

?>
